///////////////////////////////////////////////////////////////////////////////
// Main Class File:  TrainSimulator.java
// File:             Platform.java
// Semester:         CS367 Fall 2014
//
// Author:           Shane Jann jann@wisc.edu
// CS Login:         shane
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Lec 002
//
//////////////////////////// 80 columns wide //////////////////////////////////

/**
 * Represents the platform that trains may reside at. Implemented with a
 * Simple stack, so it enforces a "LIFO" style of manipulation
 * 
 * <p>Bugs: none known
 * 
 * @author Shane Jann
 */
public class Platform implements PlatformADT {

	private SimpleStack<Train> platform;
	
	/**
	 * Constructs a platform that can hold "capacity" trains
	 * 
	 * @param capacity how many trains the platform can hold
	 */
	public Platform(int capacity){
		platform = new SimpleStack<Train>(capacity);
	}
			
	/**
	 * Adds a train to the platform
	 * 
	 * @param item the Train that will be added
	 * @throws FullPlatformException is the platform is already at max
	 * 	capacity
	 */
	public void put(Train item) throws FullPlatformException {
		try {
			platform.push(item);
		} catch (FullStackException e) {
			throw new FullPlatformException();
		}
	}

	/**
	 * Returns and removes the most recently added train.
	 * 
	 * @return Train the train that is returned
	 * @throws EmptyPlatformException if the Platform has no trains in it
	 */
	public Train get() throws EmptyPlatformException {
		try {
			return platform.pop();
		} catch (EmptyStackException e) {
			throw new EmptyPlatformException();
		}
	}

	/**
	 * Checks if there is a train waiting at the platform. If so it returns
	 * the train without removing it
	 * 
	 * @return Train the most recently added train
	 * @throws EmptyPlatformException if the platform is empty
	 */
	public Train check() throws EmptyPlatformException {
		try {
			return platform.peek();
		}
		
		catch (EmptyStackException e) {
			throw new EmptyPlatformException();
		}
	}

	/**
	 * Checks if the platform is empty
	 * 
	 * @return boolean true if the platform is empty, false otherwise
	 */
	public boolean isEmpty() {
		return platform.isEmpty();
	}

	/**
	 * Checks if the platform is full
	 * 
	 * @return boolean true if the platform is full, false otherwise
	 */
	public boolean isFull() {
		return platform.isFull();
	}


}

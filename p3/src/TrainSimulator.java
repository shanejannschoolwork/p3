///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Title:            TrainSimulator.java
// Files:            Platform.java, SimipleQueue.java, SimpleStack.java,
// Semester:         CS367 Fall 2014
//
// Author:           Shane Jann
// Email:            jann@wisc.edu
// CS Login:         shane
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Lec 002
//
//////////////////////////// 80 columns wide //////////////////////////////////

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Simulates the activities of multiple trains and how they interact with
 * stack-based stations as well as queue-based train tracks.
 * 
 * <p>Bugs: Does not entirely chronologically display events (N = 0). ATD's and
 * ATA's are not accurate.
 * 
 * @author Shane Jann
 *
 */
public class TrainSimulator {


	private static ArrayList<Station> statLst = new ArrayList<Station>();
	private static ArrayList<SimpleQueue<Train>> tracks = 
			new ArrayList<SimpleQueue<Train>>();
	private static ArrayList<Train> trains = new ArrayList<Train>();
	private final static int LENGTH_OF_QUEUE = 10;


	/**
	 * Simulates the activity of the trains by reading in files, then 
	 * calculating the Actual Time of Departure (ATD) and Actual Time of 
	 * Arrival (ATA) based off of the Estimated Time of Departure (ETD) from
	 * each station. 
	 * 
	 * @param args the input of the user should be formatted like so:
	 * 	N Stations_File_Name Trains_File_Name where N is an integer between
	 *  0 and 2 inclusive.
	 */
	public static void main(String[] args) {
		if(args.length == 3){
			int userChoice = fileReader(args);  


			//Simulation of train movement
			boolean lastArrived = false;
			int currStat = 0;
			int currTrack = 0;
			while(!lastArrived){
				
				while(!statLst.get(currStat).getPlatform().isEmpty()){
					
					int currATD = 0;

					//First the trains are moved onto the tracks
					try {
						Train currTrain = 
								statLst.get(currStat).getPlatform().check();

						tracks.get(currTrack).enqueue(
								statLst.get(currStat).getPlatform().get());
						
							currATD = currTrain.getETD().get(currStat);
							currTrain.getATD().add(currATD);
							
							if(userChoice == 0){
								System.out.println(currATD + ": \t " +
							"Train " + currTrain.getId() + " has exited from" +
									" station " + (currStat + 1));
							}
						
					} catch (FullQueueException e) {
					} catch (EmptyPlatformException e) {
					} 
				}


				while(!tracks.get(currTrack).isEmpty()){

					//Next they are moved into the next station
					try {
						if(!statLst.get(currStat + 1).getPlatform().isFull()){
							Train currTrain = tracks.get(currTrack).peek();
							statLst.get(currStat + 1).getPlatform().put(
									tracks.get(currTrack).dequeue());
							
							int currATA = 0;
								currATA = currTrain.getATD().get(currStat) + 10;
								currTrain.getATA().add(currATA);
								
								if(userChoice == 0){
									System.out.println(currATA +": \t Train " +
											currTrain.getId() + " has been" +
													" parked at station " + 
											(currStat + 2));
								}
						}
						else{
							throw new FullPlatformException();
						}

					} catch (FullPlatformException e) {

						//If the next station is full, the trains in the
						//station leave so the delayed train may enter
						try {
							while(!statLst.get(currStat + 1).getPlatform().isEmpty()){

								Train currTrain = 
										statLst.get(currStat + 1).getPlatform().check();

								tracks.get(currTrack + 1).enqueue(
										statLst.get(currStat + 1).getPlatform().get());
								
								int currATD = currTrain.getETD().get(currStat + 1);
								currTrain.getATD().add(currATD);
								
								if(userChoice == 0){
									System.out.println(currATD + ": \t " +
								"Train " + currTrain.getId() + " has exited from" +
										" station " + (currStat + 2));
								}
							}
						} catch (FullQueueException e1) {
						} catch (EmptyPlatformException e1) {
						}

					} catch (EmptyQueueException e) {
					}
				}
				currStat++;
				currTrack++;
				if(statLst.get(statLst.size()-1).getPlatform().isFull()){
					lastArrived = true;
				}
				
			}
			
			//Displays ATD's
			if(userChoice == 1){
				for(int i = 0; i < trains.size(); i++){
					System.out.println(trainATD(i));
				}
			}
			
			//Displays ATA's
			if(userChoice ==2){
				for(int i = 0; i < trains.size(); i++){
					System.out.println(trainATA(i));
				}
			}


		}
		else{
			System.out.println("Usage: java TrainSimulator" +
					" N Stations_File_Name Trains_File_Name");
		}
		
	}
	
	/**
	 * Reads in the arguments passed by the user. arg[0] tells the program
	 * what to output while arg[1] and arg[2] are files including information
	 * that is to be decifered by this method.
	 * 
	 * @param args the input by the user
	 * @return int The function the user wishes for the program to do
	 * 	(i.e. 0,1, or 2. <p>0 lists off events in chronological order. 
	 *  1 displays the ATD's for each train in ascending order ID's. Likewise,
	 *  2 displays the ATA's for each train.
	 */
	private static int fileReader(String[] args){

		//The following try/catch block deals with the first argument
		//N which decides what the user wishes to do
		int N = -1;
		try{
			N = Integer.parseInt(args[0]);
			if(N == 0 || N == 1 || N == 2){

			}
			else{
				throw new IllegalArgumentException();
			}
		}
		catch(NumberFormatException e){
			System.out.println("Usage: java TrainSimulator" +
					" N Stations_File_Name Trains_File_Name");
			System.exit(0);
		}
		catch(IllegalArgumentException e){
			System.out.println("Usage: java TrainSimulator" +
					" N Stations_File_Name Trains_File_Name");
			System.exit(0);
		}

		//The following block decifers the station information file
		//as well as adds in queues to represent the tracks
		String statFileName = args[1];
		File statFile = new File(statFileName);
		Scanner statScnr;
		try {
			statScnr = new Scanner(statFile);
			int numLines = statScnr.nextInt();   
			statScnr.nextLine();

			for(int i = 0; i < numLines; i++){
				String[] statInfo = statScnr.nextLine().split(",");
				int statID = Integer.parseInt(statInfo[0]);
				int statCap = Integer.parseInt(statInfo[1]);

				//adds stations
				statLst.add(new Station(statID, statCap));

				//adds tracks
				if(statScnr.hasNext()){
					tracks.add(new SimpleQueue<Train>(LENGTH_OF_QUEUE));
				}
			}
			statScnr.close();

		} catch (FileNotFoundException e) {
			System.out.println("Error: File Not found");
			System.exit(0);
		} catch (InputMismatchException e){
			System.out.println("Input Mismatch Exception");
			System.exit(0);
		}
		//This code block reads in the train information file
		String trainFileName = args[2];
		File trainFile = new File(trainFileName);
		Scanner trainScnr;
		ArrayList<Train> trainList = new ArrayList<Train>();

		try{
			trainScnr = new Scanner(trainFile);
			int numLines = trainScnr.nextInt();   //the number of lines in the file
			trainScnr.nextLine();


			for(int i = 0; i < numLines; i++){
				String[] trainInfo = trainScnr.nextLine().split(",");
				Train train = new Train(Integer.parseInt(trainInfo[0]));
				trains.add(train);

				for(int j = 1; j < trainInfo.length; j++){
					train.getETD().add(Integer.parseInt(trainInfo[j]));

				}
				

				trainList.add(train);
			}
			trainScnr.close();
		} catch (FileNotFoundException e) {
			System.out.println("Error: File Not found");
			System.exit(0);
		} catch (InputMismatchException e){
			System.out.println("Input Mismatch Exception");
			System.exit(0);
		}

		//puts trains into the first station
		for(int i = trainList.size() - 1; i >= 0; i--){
			try {
				statLst.get(0).getPlatform().put(trainList.get(i));
			} catch (FullPlatformException e) {
				System.exit(0);
			}
		}


		return N;
	}
	
	/**
	 * Helper method that displays the ATD of one train
	 * 
	 * @param trainNum train with the desired ATD's
	 * @return List<Integer> the ATD's returned
	 */
	private static List<Integer> trainATD(int trainNum){
		return trains.get(trainNum).getATD();
	}
	
	/**
	 * Helper method that displays the ATA of one train
	 * 
	 * @param trainNum train with the desired ATA's
	 * @return List<Integer> the ATA's returned
	 */
	private static List<Integer> trainATA(int trainNum){
		return trains.get(trainNum).getATA();
	}

}

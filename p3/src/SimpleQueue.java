///////////////////////////////////////////////////////////////////////////////
// Main Class File:  TrainSimulator.java
// File:             SimpleQueue.java
// Semester:         CS367 Fall 2014
//
// Author:           Shane Jann jann@wisc.edu
// CS Login:         shane
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Lec 002
//
//////////////////////////// 80 columns wide //////////////////////////////////

/**
 * Data Structure that represents a Queue ("First In First Out"). Implemented
 * with an array.
 *
 * <p>Bugs: none known
 *
 * @author Shane Jann
 */
public class SimpleQueue<E> implements QueueADT<E> {

	private E[] items;
	private int numItems;
	private int capacity;
	private int front;
	
	
	
	@SuppressWarnings("unchecked")
	/**
	 * Constructs a SimpleQueue of size capacity. 
	 * 
	 * @param capacity size of queue
	 * @throws IllegalArgumentException if capacity is negative
	 */
	public SimpleQueue(int capacity){
		if(capacity < 0){
			throw new IllegalArgumentException();
		}
		
		this.capacity = capacity;
		this.numItems = 0;
		items = (E[]) new Object[capacity];
		
	}
	
	/**
	 * Adds an item to the back of the line (rear of the queue).
	 * 
	 * @param item the item of generic type E that is to be added
	 * @throws FullQueueException if the queue is maxed out
	 */
	public void enqueue(E item) throws FullQueueException{
		if(isFull()){
			throw new FullQueueException();
		}
		
		this.items[numItems] = item;
		
		numItems++;
	}
	
	/**
	 * Removes and returns an item at the front of the line 
	 * (front of the queue).
	 * 
	 * @return E the item of generic type E that is to be returned
	 * @throws EmptyQueueException if the Queue has no such items in it
	 */
	public E dequeue() throws EmptyQueueException{
		if(isEmpty()){
			throw new EmptyQueueException();
		}
		
		E item = items[front];
		
		numItems--;
		front++;
		return item;
	}
	
	/**
	 * Checks what is at the front of the queue without removing it.
	 * 
	 * @return E the item at the front of the queue
	 * @throws EmptyQueueException if the queue has no items in it
	 */
	public E peek() throws EmptyQueueException{
		if(isEmpty()){
			throw new EmptyQueueException();
		}
		return items[front];
	}
	
	/**
	 * Checks if the queue is empty.
	 * 
	 * @return boolean true if empty, false otherwise
	 */
	public boolean isEmpty(){
		return numItems == 0;
	}
	
	/**
	 * Checks if the queue is full
	 * 
	 * @return boolean true if full, false otherwise
	 */
	public boolean isFull(){
		return numItems == capacity;
	}
}

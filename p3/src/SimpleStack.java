///////////////////////////////////////////////////////////////////////////////
// Main Class File:  TrainSimulator.java
// File:             SimpleStack.java
// Semester:         CS367 Fall 2014
//
// Author:           Shane Jann jann@wisc.edu
// CS Login:         shane
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Lec 002
//
//////////////////////////// 80 columns wide //////////////////////////////////

/**
 * Data Structure that acts like a "LIFO" (Last in first out). Implemented
 * with an array. This data structure cannot be resized.
 * 
 * <p>Bugs: none known
 * 
 * @author Shane Jann
 */
public class SimpleStack<E> implements StackADT<E> {
	private E[] items;      //items in the stack
	private int numItems;   
	private int capacity;   //the final size of the stack
	
	
	
	
	@SuppressWarnings("unchecked")
	/**
	 * Constructs a stack of a particular type with the size capacity
	 * 
	 * @param capacity size of the stack
	 */
	public SimpleStack(int capacity){
		if(capacity < 0){
			throw new IllegalArgumentException();
		}
		
		this.capacity = capacity;
		items = (E[]) new Object[capacity];
		this.numItems = 0;
	
	}
	
	/**
	 * Adds an item to the stack.
	 * 
	 * @param item the generic item that will be added
	 * @throws FullStackException if the stack is at max capacity
	 */
	public void push(E item) throws FullStackException{
		if(isFull()){
			throw new FullStackException();
		}
		
		this.items[numItems] = item;
		numItems++;
	}
	
	/**
	 * The most recent item will be removed from the stack and returned
	 * 
	 * @return E the most recently added generic item that will be returned
	 * @throws EmptyStackException if there are no items to return
	 */
	public E pop() throws EmptyStackException{
		if(isEmpty()){
			throw new EmptyStackException();
		}
		
		E item = items[numItems - 1];
		numItems--;
		
		return item;
	}
	
	/**
	 * The most recent item will be returned.
	 * 
	 * @return E the most recently added generic item that will be returned
	 * @throws EmptyStackException if there are no items to return
	 */
	public E peek() throws EmptyStackException{
		if(isEmpty()){
			throw new EmptyStackException();
		}
		
		return items[numItems-1];
	}
	
	/**
	 * Checks if the stack is empty
	 * 
	 * @return boolean true if Empty, false otherwise
	 */
	public boolean isEmpty(){
		return numItems == 0;
	}
	
	/**
	 * Checks if the stack is full
	 * 
	 * @return boolean true if Full, false otherwise
	 */
	public boolean isFull(){
		return numItems == capacity;
	}

}
